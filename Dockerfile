FROM node:current-alpine
RUN npm install -g @aws-amplify/cli
ENTRYPOINT [ "amplify" ]